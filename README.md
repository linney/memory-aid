# Memory Aid

## A tool to help you remember things
Using knowledge of a [forgetting curve](https://www.wikiwand.com/en/Forgetting_curve) we can help the user remember things easier.
