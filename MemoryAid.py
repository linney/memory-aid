import json
import uuid
import dateutil.parser
from datetime import datetime, timedelta


class MemoryAid:
    memory_file_name = 'memories.json'

    def __init__(self):
        super().__init__()
        try:
            with open(self.memory_file_name, 'r') as memory_file:
                self.memories = json.load(memory_file)
        except FileNotFoundError:
            self.memories = {}

    def getMemories(self, all=True):
        if all:
            return self.memories
        else:
            memoryDict = {}
            for key, memory in self.memories.items():
                if dateutil.parser.parse(memory['next_access']) < datetime.now():
                    memoryDict[key] = memory
            return memoryDict

    def updateMemory(self):
        """Updates our json file to match our internal memory"""
        with open(self.memory_file_name, 'w') as memory_file:
            json.dump(self.memories, memory_file)

    def addMemory(self, title: str, value: str):
        """Adds an item to our 'memory'."""
        self.memories[str(uuid.uuid4())] = {'title': title, 'value': value, 'last_access': datetime.now().isoformat(), 'next_access': datetime.now().isoformat(), 'memory_level': 0}
        self.updateMemory()

    def determineNextMemoryReview(self, level: int):
        options = {
                0: datetime.now,
                1: datetime.now,
                2: datetime.now,
                3: lambda: datetime.now() + timedelta(days=1),
                4: lambda: datetime.now() + timedelta(days=3),
                5: lambda: datetime.now() + timedelta(days=7),
                6: lambda: datetime.now() + timedelta(days=14),
                7: lambda: datetime.now() + timedelta(days=14)
                }
        if level >= 8:
            return datetime.now() + timedelta(days=30)
        else:
            return options[level]();

    def getNextMemoryToText(self):
        """Gets the next available memory"""
        for key, memory in self.memories.items():
            if dateutil.parser.parse(memory['next_access']) < datetime.now():
                return key, memory

    def resetMememorisations(self, memory_key):
        """Resets the memorisation of a memory"""
        if memory_key in self.memories:
            self.memories[memory_key]['memory_level'] = 0
            self.memories[memory_key]['next_access'] = self.determineNextMemoryReview(self.memories[memory_key]['memory_level']).isoformat()
            self.updateMemory()
        else:
            raise KeyError("The memory was not found in your list of memories.")


    def increaseMemorisation(self, memory_key):
        """Increases the memorisation level of a memory"""
        if memory_key in self.memories:
            self.memories[memory_key]['memory_level'] += 1
            self.memories[memory_key]['next_access'] = self.determineNextMemoryReview(self.memories[memory_key]['memory_level']).isoformat()
            self.updateMemory()
        else:
            raise KeyError("The memory was not found in your list of memories.")
